export default {
  saveTests(state, payload) {
    state.tests = payload;
  },
  saveTest(state, payload) {
    // debugger
    const questions = payload.questions.map((item, i) => ({ ...item, id: i + 1 }))
    state.editTest = {...payload, questions};
  }
};
