import api from "../../../api"

export default {
  async loadTests({ commit, rootState }) {
    const { _id } = rootState.user;
    const res = await api.teacher.getTests({ 'author._id': _id });

    commit('saveTests', res.answer);

    return res;
  },
  async loadTest({ commit }, payload) {
    const res = await api.teacher.getTest(payload);

    commit('saveTest', res.answer);

    return res;
  },
  async updateTest(payload) {
    const res = await api.teacher.updateTest(payload);

    return res;
  },
  async deleteTestById(_, payload) {
    const res = await api.teacher.deleteTest(payload);

    return res;
  }
};
