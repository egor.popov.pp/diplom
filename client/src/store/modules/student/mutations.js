export default {
  saveTestInfo(state, payload) {
    state.testBaseInfo = payload;
  },
  saveTest(state, payload) {
    state.test = payload;
  },
  mutateTestInfo(state, payload) {
    state.testBaseInfo = { ...state.testBaseInfo, resultId: payload };
  }
};
