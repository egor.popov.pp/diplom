import api from "../../../api"

export default {
  async getTest({ commit }, payload) {
    const res = await api.student.getTest(payload);

    commit('saveTestInfo', res.answer);

    return res;
  },
  // TODO возможно стоит перенести в store/modules/tester
  async startTest({ commit }, payload) {

    const res = await api.student.startTest(payload);

    commit('tester/saveQuestion', { question: res.answer.question, number: 0 }, { root: true });
    commit('mutateTestInfo', res.answer.resultId);
    return res;
  },
  async loadNextQuestion({ commit }, payload) {
    const res = await api.student.getQuestion(payload);

    commit('tester/saveQuestion', res.answer, { root: true });
  },
  async sendAnswer({ commit }, payload) {
    const res = await api.student.sendAnswer(payload);
    if (res.answer !== 'end') {
      commit("tester/saveQuestion", res.answer, { root: true })
    } else {
      commit("tester/endTest", true, { root: true });
    } 
    return res;
  }
};
