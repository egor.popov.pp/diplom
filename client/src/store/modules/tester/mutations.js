export default {
  saveQuestion(state, payload) {
    state.question = payload.question;
    state.curQuestionNumber = payload.number;
  },
  endTest(state, payload) {
    state.ended = payload;
  }
}