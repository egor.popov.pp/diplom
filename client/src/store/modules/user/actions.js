export default {
  setTokens({ commit }, tokens) {
    localStorage.setItem('auth', JSON.stringify(tokens));
    commit('setTokens', tokens);
  },
  clearTokens({ commit }) {
    localStorage.removeItem('auth');
    commit('clearTokens');
  },
  initTokens({ commit }) {
    const auth = localStorage.getItem('auth');
    if (auth) {
      commit('setTokens', JSON.parse(auth));
    } 
  }
}
