import decode from 'jwt-decode';


export default {
  setTokens(store, { access, refresh }) {
    const { name, role, _id } = decode(access);
    store.name = name;
    store.role = role;
    store._id = _id;
    store.access = access;
    store.refresh = refresh;
  },
  clearTokens(store) {
    Object.keys(store).forEach(key => store[key] = '');
  }
}