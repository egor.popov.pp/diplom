// https://vuex.vuejs.org/en/mutations.html

export default {
  setMessageText(state, payload) {
    const { text, type } = payload;
    state.message = {
      text,
      type,
    }
  },
  saveFallback(state, payload) {
    state.fallback = payload;
  },
  saveLoadingState(state, payload) {
    state.loading = payload;
  },
};

