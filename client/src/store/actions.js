// https://vuex.vuejs.org/en/actions.html

import api from "../api";

export default {
  setMessageText({ commit }, payload) {
    commit('setMessageText', payload);
  },
  setFallback({ commit }, payload) {
    commit('saveFallback', payload);
  },
  setLoadingState({ commit }, payload) {
    commit('saveLoadingState', payload);
  },
  async createTest(_, payload) {
    const res = await api.teacher.createTest(payload);
    return res;
  }
}
