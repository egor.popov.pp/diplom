import testRoutes from './test'
import userRoutes from './user'

export default [
  ...testRoutes,
  ...userRoutes
]
