import Role from "@/utils/Role";

export default [
  {
    path: "/test",
    name: "Test",
    component: () => import("@/views/Test.vue"),
    meta: {
      access: [Role.Teacher],
      middleware: ["auth", "access"],
    },
    children: [
      {
        path: "create",
        name: "TestCreate",
        component: () => import("@/views/TestCreate.vue"),
        meta: {
          access: [Role.Teacher],
          middleware: ["auth", "access"],
        },
      },
      {
        path: "list",
        name: "TestList",
        component: () => import("@/views/TestList.vue"),
        meta: {
          access: [Role.Teacher, Role.Student],
          middleware: ["auth", "access"],
        },
      },
      {
        path: "list/:id",
        name: "TestEdit",
        component: () => import("@/views/TestEdit.vue"),
        meta: {
          access: [Role.Teacher],
          middleware: ["auth", "access"],
        },
      },
      {
        path: ":id/pass",
        name: "TestPass",
        component: () => import("@/views/TestPass.vue"),
        meta: {
          access: [Role.Student],
          middleware: ["auth", "access"],
        },
      },
      {
        path: ":id/result",
        name: "TestPass",
        component: () => import("@/views/TestResults.vue"),
        meta: {
          access: [Role.Teacher],
          middleware: ["auth", "access"],
        },
      },
      // {
      //   path: 'list/:id',
      //   name: 'TestEdit',
      //   component: () => import('@/views/TestEdit.vue'),
      //    meta: {
      //     access: [Role.Teacher],
      //     middleware: ['auth', 'access'],
      //   }
      // },
      // {
      //   path: ''
      // },
    ],
  },
];
