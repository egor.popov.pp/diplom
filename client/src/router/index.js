
import Vue from 'vue'

import Router from 'vue-router'
import Meta from 'vue-meta'
import MyRoutes from './routes'

import middlewares from './middlewares';

import store from '@/store'

Vue.use(Router)
Vue.use(Meta)
// Create a new router
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('@/views/Dashboard.vue'),
      meta: {
        access: '*',
        middleware: ['auth'],
      }
    },
    {
      path: '/auth',
      name: 'Auth',
      component: () => import('@/views/Auth.vue'),

    },
    {
      path: '/registration',
      name: 'Registration',
      component: () => import('@/views/Registration.vue')
    },
    {
      path: '/results',
      name: 'Results',
      component: () => import('@/views/Results.vue')
    },
    ...MyRoutes
  ]
})

const pipe = async (arr, ctx) => {
  for await (const f of Object.keys(middlewares)) {
    middlewares[f](ctx);
  }
};

router.beforeEach((to, from, next) => {
  const { fallback } = store.state;
  const { access }   = store.state.user;

  if (access && fallback && fallback !== to.path) {
    store.dispatch('setFallback', null);
    return next(fallback)
  }

  const routeMiddlewares = to.meta.middleware;

  if (!routeMiddlewares) {
    return next();
  }


  const chain = Object.keys(middlewares).filter(x => routeMiddlewares.includes(x));
  const ctx = {
    store,
    to,
    from,
    next,
  };

  pipe(chain, ctx);
});



export default router
