import auth from './auth';
import access from './access';

export default {
  auth,
  access,
};
