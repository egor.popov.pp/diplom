import Role from '@/utils/Role';

export default function ({ store, next, to }) {
  const { role } = store.state.user;

  if (to?.meta?.access?.includes(role) || to?.meta?.access === '*') {
    next();
    return;
  }
  next();
}
