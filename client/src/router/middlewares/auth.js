export default function ({ store, next, to, from }, options) {
  const token = store.state.user.access;

  if (!token) {
    store.dispatch('setFallback', to.path);
    return next('/auth');    
  }
  next();
}
