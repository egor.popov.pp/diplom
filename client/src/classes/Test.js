class Test {
  static statuses = {
    'finished': 'Завершен',
    'in_progress': 'В процессе',
    'new': 'Новый'
  }
  static getTestStatusByCode(code) {
    return this.statuses[code];
  }
}

export default Test;