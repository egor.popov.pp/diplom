import api from "../api"

function plugin(ctx) {
  return function (store) {
    ctx.Store.prototype.$api = api;
  }
}

export default plugin;