import {
  ValidationProvider,
  ValidationObserver,
  extend,
  setInteractionMode,
} from 'vee-validate';

import { required, email } from 'vee-validate/dist/rules';

export default {
  components: {
    ValidationObserver,
    ValidationProvider,
  },
  props: {
    viewMode: {
      type: Boolean,
      default: () => false,
    },
    readonly: {
      type: Boolean,
      default: () => false,
    },
    value: {
      type: Object,
      default: () => null,
    },
    config: {
      type: Object,
      default: () => null,
    },
    controlConfig: {
      type: Object,
      default: () => {}
    }
  },
  data() {
    return {
      innerConfig: this.value || {},
    }
  },
  watch: {
    value: {
      deep: true,
      handler(v) {
        this.innerConfig = v;
      },
    },
    innerConfig: {
      deep: true,
      handler(v) {
        this.$emit('input', v);
      }
    },
    result: {
      deep: true,
      handler(v) {
        this.innerConfig.result = v;
      }
    }
  },
  created() {
    setInteractionMode('eager');
    extend('required', {
      ...required,
      message: 'Это поле обязательно',
    });
  },
};
