import { mapState } from 'vuex';
export default {
  render: function( h ) {
      const { component } = this;
      return h(component)
  },
  computed: {
    ...mapState('user', ['role']),
    componentName() {
      return this.$options.name;
    },
    component() {
      return () =>  import(`@/components/pages/${this.componentName}/${this.role}`)
    }
  },

}