import {
  ValidationProvider,
  ValidationObserver,
  extend,
  setInteractionMode,
} from 'vee-validate';

import { required, email } from 'vee-validate/dist/rules';
export default {
  components: {
    ValidationObserver,
    ValidationProvider,
  },
  created() {
    setInteractionMode('eager');
    extend('required', {
      ...required,
      message: 'Это поле обязательно',
    });
  }
}