import axios from './axios';

export default async (payload) => {
  const res = await axios.post('/', payload);
  return res.data;
}