import query from './query'

export default {
  common: {
    /* войти
      data = { login, password }
      return { access, refresh }
    */
    login: (data) => query({ data, method: 'login'}),

    /* зарегистрироваться
      data = { login, password, role }
    */
    registration: (data) => query({ data, method: 'registration'}),

    /* обновить токены
      data = { refresh }
      return { access, refresh }
    */
    refresh: (data) => query({ data, method: 'refresh'}),
  },
  teacher: {
    // создать тест data = test
    createTest: (data) => query({ data, method: 'createTest'}),

    // обновить тест data = test
    updateTest: (data) => query({ data, method: 'updateTest'}),
    
    // удалить тест data = { _id }
    deleteTest: (data) => query({ data, method: 'deleteTest'}),

    /* получить список тестов
      data = объект запроса для mongoose
      return [test]
    */
    getTests: (data) => query({ data, method: 'getTests'}),

    /* получить один тест
      data = { _id }
      return test
    */
    getTest: (data) => query({ data, method: 'getTest'}),

    /* получить группы преподавателя
      return [teacher.group]
    */
    getGroups: () => query({ data: {}, method: 'getGroups'}),

    /* сохранить группы преподавателя
      data = { groups }
    */
    saveGroups: (data) => query({ data, method: 'saveGroups'}),

    /* получить результаты по тесту
    data = { testId }
      return [result]
    */
    getResults: (data) => query({ data, method: 'getResults'}),
  },
  student: {
    /* получаем базовую инфу
      data = { testId }
      return { author, name, number, status, groups, resultId }
    */
    getTest: (data) => query({ data, method: 'getTest'}),

    /* начать тест
      data = { group, testId }
      return { question, resultId }
    */
    startTest: (data) => query({ data, method: 'startTest'}),

    // удалить результаты data = { resultId } 
    resetTest: (data) => query({ data, method: 'resetTest'}),

    // закончить тест data = { resultId } 
    endTest: (data) => query({ data, method: 'endTest'}),

    /* отправить ответ
      data = { value, resultId } 
      return { question, number }
    */
    sendAnswer: (data) => query({ data, method: 'sendAnswer'}),

    /* пропустить вопрос
      data = { resultId } 
      return { question, number }
    */
    skipQuestion: (data) => query({ data, method: 'skipQuestion'}),

    /* получить вопрос, например для продолжения начатого ранее теста
      data = { resultId } 
      return { question, number }
    */
    getQuestion: (data) => query({ data, method: 'getQuestion'}),

    /* получить результаты
      return [result]
    */
    getResults: () => query({ data: {}, method: 'getResults'}),
  }
} 