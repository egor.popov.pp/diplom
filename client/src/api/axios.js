import axios from 'axios';
import api from './index'
import store from '@/store';
import router from '@/router'

const axiosIns = axios.create({
  baseURL: process.env.VUE_APP_BACKEND_URL,
  timeout: 15000,
  // withCredentials: true,
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json',
  },
});

axiosIns.interceptors.request.use(async function (config) {
  await store.dispatch('setLoadingState', true);
  const { user } = store.state;
  config.headers.common["Authorization"] = user.access;
  return config;
}, function (error) {
  return Promise.reject(error);
});


axiosIns.interceptors.response.use(async function (response) {
  await store.dispatch('setLoadingState', false);
  console.log(response)
  const { err } = response.data;
  switch (err) {
    case 'Токен обновления не действителен':
      router.push({ name: 'Auth' });
      return response;

    case 'Токен доступа не действителен':
      await api.common.refresh({ 
        refresh: store.state.user.refresh,
      });
      response.config.headers.Authorization = store.state.user.access;
      
      return await axios.request(response.config);

    case null:
      const { access, refresh } = response.data.answer;
      if (!(access && refresh)) return response;
      
      await store.dispatch('user/setTokens', response.data.answer);
  
      return response;

    default:
      store.dispatch('setMessageText', {
        text: err,
        type: 'error'
      });
      return response;
  }  
}, async function (error) {
  console.log(error);
  await store.dispatch('setLoadingState', false);
  store.dispatch('setMessageText', {
    text: 'Неизвестная ошибка!',
    type: 'error'
  });
  return Promise.reject(error);
});

export default axiosIns;