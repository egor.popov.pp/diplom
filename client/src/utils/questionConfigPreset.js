import { VTextField, VSelect } from "vuetify/lib";

const config = {
  name: {
    component: VTextField,
    label: "Текст вопроса"
  },
  questionType: {
    component: VSelect,
    'full-width': true,
    label: 'Выбирите тип вопроса',
    items: [
      {
        text: "Вопрос с выбором одного правильного ответа",
        value: "simple"
      },
      {
        text: "Вопрос с выбором нескольких правильных ответов",
        value: "simple-few-variants"
      },
      {
        text: "Вопрос с выбором одного неправильного ответа",
        value: "simple-one-fail"
      },
      {
        text: "Вопрос на установление соответствия",
        value: "conformity"
      },
      {
        text: "Вопрос на установление последовательности",
        value: "sequencing"
      },
      {
        text: "Вопрос с откырой формой ответа",
        value: "open-answer"
      },
    ]
  },
  // taskByType: {
  //   component: taskProvider,
  // }
};

export default config;