export async function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    prompt("Ссылка на тест", text);
    return
  }
  let res;
  try {
    res = await navigator.clipboard.writeText(text);
  } catch(err) {
    res = err;
  }
  return res;
}