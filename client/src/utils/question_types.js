export default [
  {
    text: "Вопрос с выбором одного правильного ответа",
    value: "simple",
  },
  {
    text: "Вопрос с выбором нескольких правильных ответов",
    value: "simple-few-variants",
  },
  {
    text: "Вопрос с выбором одного неправильного ответа",
    value: "simple-one-fail",
  },
  // {
  //   text: "Вопрос на установление соответствия",
  //   value: "conformity",
  // },
  // {
  //   text: "Вопрос на установление последовательности",
  //   value: "sequencing",
  // },
  {
    text: "Вопрос с откырой формой ответа",
    value: "open-answer",
  },
];
