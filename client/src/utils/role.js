const Role = {
  Admin: 'admin',
  Teacher: 'teacher',
  Student: 'student'
}

export default Role
