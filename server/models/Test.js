const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    name: String,
    author: {
      name: String,
      _id: Schema.Types.ObjectId,
    },
    maxPoints: Number,
    groups: [String], // группы на выбор для индефикации учащегося
    type: String, // тип теста.  адаптивный, простой итд
    questions: [{
      type: { type: String }, // тип задания
      result: Schema.Types.Mixed, // правильный ответ
      variants: Schema.Types.Mixed, // варианты ответа
      condition: Schema.Types.Mixed, // текст вопроса или данные для условия
      point: Number, // баллы за задание
    }],
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.model('Test', schema);
