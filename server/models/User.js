const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    name: String,
    email: String,
    password: String,
    role: String,
    groups: [String]
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.model('User', schema);