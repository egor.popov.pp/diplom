const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema(
  {
    finished: Boolean,
    test: Object,
    list: [{
      type: { type: String }, // тип задания
      result: Schema.Types.Mixed, // правильный ответ
      variants: Schema.Types.Mixed, // варианты ответа
      condition: Schema.Types.Mixed, // текст вопроса или данные для условия
      point: Number, // баллы за задание
    }],
    points: Number,
    answers: [{
      questionId: Schema.Types.ObjectId,
      value: Object,
      correct: Boolean
    }],
    student: {
      name: String,
      _id: Schema.Types.ObjectId,
      group: String
    },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.model('Result', schema);