const Test = require('../models/Test');
const User = require('../models/User');
const Result = require('../models/Result');
const mongoose = require('mongoose');

module.exports.createTest = async (data) => {
  data.maxPoints = data.questions.reduce((sum, q) => sum + q.point, 0);
  await Test.create(data);
} 

module.exports.updateTest = async (data) => {
  data.maxPoints = data.questions.reduce((sum, q) => sum + +q.point);
  await Test.updateOne({ _id: new mongoose.Types.ObjectId(data._id) }, data);
} 

module.exports.deleteTest = async ({ _id }) => {
  await Test.deleteOne({ _id: new mongoose.Types.ObjectId(_id) });
} 

module.exports.getTests = (data) => Test.find(data);

module.exports.getTest = ({ _id }) => Test.findById(_id);

module.exports.getGroups = async (data, user) => {
  const teacher = await User.findById(user._id);
  return teacher.groups || [];
}

module.exports.saveGroups = async ({ groups }, user) => {
  await User.updateOne({ _id: new mongoose.Types.ObjectId(user._id) }, { groups });
}

module.exports.getResults = async ({ testId }) => {
  return await Result.find({ 'test._id': new mongoose.Types.ObjectId(testId) }).select('-test.questions -list');
}
  
