const bcrypt = require('bcryptjs');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const createTokens = require('../utils/createTokens');

module.exports.login = async (data) => {
  const user = await User.findOne({ email: data.email });
  if (!user) throw new Error('Аккаунта не существует');
  const match = await bcrypt.compare(data.password, user.password);
  if (!match) throw new Error('Пароль не верный');
  return createTokens(user);
}

module.exports.registration = async (data) => {
  const user = await User.findOne({ email: data.email });
  if (user) throw new Error('Аккаунт уже существует');
  data.password = await bcrypt.hash(data.password, 10);
  await User.create(data);
}

module.exports.refresh = async (data) => {
  const decoded = jwt.verify(data.refresh, process.env.JWT_SECRET_KEY);
  if (Math.floor(Date.now() / 1000) > decoded.exp) {
    throw new Error('Токен обновления не действителен');
  } 
  const user = await User.findById(decoded._id);
  if (!user) throw new Error('Ошибка обновления токена');
  return createTokens(user);
}