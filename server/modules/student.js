const Test = require('../models/Test');
const Result = require('../models/Result');
const User = require('../models/User');
const mongoose = require('mongoose');

function getQuestion(list) {
  const question = list[0];
  question.result = {};
  return question;
}

module.exports.startTest = async ({ group, testId }, user) => {
  const test = await Test.findById(testId);
  if (!test) throw new Error('Теста не существует');

  const result = await Result.findOne({ 
    'student._id': new mongoose.Types.ObjectId(user._id),
    'test._id': new mongoose.Types.ObjectId(testId)
  });
  if (result) throw new Error('Тест уже начат');

  const newResult = await Result.create({
    test,
    finished: false,
    list: test.questions,
    points: 0,
    student: {
      group,
      _id: new mongoose.Types.ObjectId(user._id),
      name: user.name
    }
  });

  return { 
    question: getQuestion(test.questions),
    resultId: newResult._id
  }
};

module.exports.skipQuestion = async ({ resultId }) => {
  const result = await Result.findById(resultId);
  result.list.push( result.list.shift() );
  return {
    number: result.test.questions.length - result.list.length,
    question: getQuestion(result.list)
  } 
};

module.exports.getQuestion = async ({ resultId }) => {
  const result = await Result.findById(resultId);
  return {
    number: result.test.questions.length - result.list.length,
    question: getQuestion(result.list)
  } 
}

module.exports.sendAnswer = async ({ value, resultId }) => {
  const result = await Result.findById(resultId);
  const question = result.list.shift();
  let correct = false;

  switch (question.type) {
    case 'simple':
    case 'simple-one-fail':
      if (value === question.result) correct = true;
      break;
    case 'simple-few-variants':
      if ( 
        value.length === question.result.length
        && value.every(v => question.result.includes(v))
      ) correct = true;
      break;
    case 'open-answer':
      const val = value.toLowerCase().trim();
      const res = question.result.toLowerCase().trim();
      if (val === res) correct = true;
      break;
    default:
      throw new Error('Неизвестный тип вопроса');
    }

  if (correct) result.points = result.points + question.point;
  result.answers.push({ value, correct, questionId: question._id });
  if (!result.list.length) result.finished = true;

  await Result.updateOne({ _id: new mongoose.Types.ObjectId(result._id) }, result);

  if (!result.list.length) return 'end';
  return {
    number: result.test.questions.length - result.list.length,
    question: getQuestion(result.list)
  } 
};

module.exports.resetTest = async ({ resultId }) => {
  await Result.deleteOne({ _id: new mongoose.Types.ObjectId(resultId) });
} 

module.exports.endTest = async ({ resultId }) => {
  await Result.updateOne({ _id: new mongoose.Types.ObjectId(resultId) }, { finished: true });
} 

module.exports.getTest = async ({ testId }, user) => {
  const result = await Result.findOne({ 
    'student._id': new mongoose.Types.ObjectId(user._id),
    'test._id': new mongoose.Types.ObjectId(testId)
  });

  let test = {};
  let status = '';

  if (result) {
    test = result.test;
    status = result.finished ? 'finished' : 'in_progress';
  } else {
    test = await Test.findById(testId).select('name author questions');
    status = 'new';
  }
 
  const teacher = await User.findById(test.author._id);

  return {
    author: test.author,
    name: test.name,
    number: test.questions.length,
    groups: teacher.groups,
    resultId: result ? result._id : null,
    status
  }
}

module.exports.getResults = async (data, user) => {
  return await Result.find({ 'student._id': user._id }).select('-test.questions -list');
}