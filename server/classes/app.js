const Koa = require('koa');
const cors = require('@koa/cors');
const koaBody = require('koa-body');
const mongoose = require('mongoose');
const dir = require('require-dir');
const db = dir('../models');
const Api = require(`./api`);
const api = new Api();

require('dotenv').config();

module.exports = class App extends Koa {
  initContextMethods () {
    this.context.answer = function (data) {
      this.body = { err: null, answer: data || 'ok'};
    }

    this.context.err = function (data) {
      this.body = { err: data || 'error', answer: null };
    }
  }

  async initDB () {
    await mongoose.connect(
      process.env.DB_URL, 
      {
        useNewUrlParser: true,
        useUnifiedTopology: true 
      }
    );

    this.context.db = db;
  }

  initMiddleware () {
    this.use(cors());
    this.use(koaBody());
    this.use( api.validator() );
    this.use( api.handler() );
  }

  async start(port) {
    await this.initDB();
    this.initContextMethods();
    this.initMiddleware();
    this.listen(port);
    console.log('Сервер запущен на порту: ' + port);
  }
}