const { join } = require('path');
const requreDir = require('require-dir');
const jwt = require('jsonwebtoken');

module.exports = class Api {
  constructor () {
    this.modules = requreDir(join(process.cwd(), 'modules'))
  }

  validator () {
    return async (ctx, next) => {
      const { method } = ctx.request.body;
      const access = ctx.request.header.authorization;

      if (!method) {
        ctx.body = 'Сервер работает';
        return;
      }
    
      if (access && method !== 'refresh') {
        try {
          const decoded = await jwt.verify(access, process.env.JWT_SECRET_KEY);
          ctx.state.user = {
            role: decoded.role,
            _id: decoded._id,
            name: decoded.name
          };
        } catch(err) {
          ctx.err('Токен доступа не действителен');
          return;
        }
      } else {
        ctx.state.user = { role: 'common' };
      }

      if (!this.modules[ctx.state.user.role][method]) {
        ctx.err('Метод не найден для данной роли');
        return;
      }   
      return next();
    }
  }

  handler () {
    return async ctx => {
      let { method, data } = ctx.request.body;
      const { user } = ctx.state;
      try {
        const res = await this.modules[user.role][method](data, user);
        ctx.answer(res);
      } catch (e) {
        ctx.err(e.message);
      };
    };
  }
}