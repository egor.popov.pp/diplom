const jwt = require('jsonwebtoken');

module.exports = (user) => {
  const access = {
    _id: user._id,
    name: user.name,
    role: user.role,
    type: 'access',
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
  };

  const refresh = {
    _id: user._id,
    type: 'refresh',
    exp: Math.floor(Date.now() / 1000) + (60*60*48),
  };

  return {
    access: jwt.sign(access, process.env.JWT_SECRET_KEY),
    refresh: jwt.sign(refresh, process.env.JWT_SECRET_KEY),
  }
}