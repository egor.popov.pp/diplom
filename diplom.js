const { exec } = require('child_process');
const { join } = require('path');

function log(str) {
  console.log(`<<< diplom >>> ${str}`);
}

function installPromise(folder) {
  return new Promise((resolve) => {
    exec(
      'npm i',
      {
        cwd: join(__dirname, folder),
        windowsHide: true,
      },
      (err) => { 
        if (err) log(`Ошибка при установке зависимостей ${folder}`)
        else log(`Зависимости ${folder} установлены...`)
        resolve(err);
      } 
    );
  });
}

async function install() {
  try { 
    log('Установка зависимостей...');
    let promises = [
      installPromise('server'),
      installPromise('client')
    ];
    let errors = await Promise.all(promises);
    log('Установка завершена');
    errors.forEach(e => e ? console.log(e): null);
  } catch (e) {
    throw e;
  }
}

async function main() { 
  if (process.argv.includes('-i')) await install();
  process.exit(0);
}

main();